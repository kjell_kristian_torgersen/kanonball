package kanonBall;

import java.awt.Color;
import java.awt.Graphics;

public class Kule implements IEntity
{

	double _x, _y;
	int _radius;
	double _vx;
	boolean _falling = false;
	Color _col;

	public Kule(int x, int y, int radius, Color col, boolean falling)
	{
		_x = (double) x;
		_y = (double) y;
		_radius = radius;
		_col = col;
		_falling = falling;
	}

	public void Turn()
	{
		_falling = !_falling;
	}

	public void Iter(double dt)
	{
		if (_falling) {
			_y += 200.0 * dt;
		} else {
			_y += -200.0 * dt;
		}
		_x += _vx;

		_vx *= 0.99;

		if (_x <= 0) {
			_vx = -_vx;
		}

		if (_x >= 512) {
			_vx = -_vx;
		}

		if (_y > 768 - (double) _radius) {
			_falling = false;
		}

		if (_y < _radius) {
			_falling = true;
		}
	}

	public void Draw(Graphics g)
	{
		g.setColor(_col);
		g.fillOval((int) _x - _radius, (int) _y - _radius, 2 * _radius, 2 * _radius);
		g.setColor(Color.BLACK);
		g.drawOval((int) _x - _radius, (int) _y - _radius, 2 * _radius, 2 * _radius);
	}

	public int X()
	{
		return (int) _x;
	}

	public int Y()
	{
		return (int) _y;
	}

	public int R()
	{
		return (int) _radius;
	}

	public boolean Falling()
	{
		return _falling;
	}

	public double VX()
	{
		return _vx;
	}

	public void Add_vx(double _vx)
	{
		this._vx += _vx;
	}
}
