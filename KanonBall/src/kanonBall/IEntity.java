package kanonBall;

import java.awt.Graphics;

public interface IEntity
{
	public int X();
	public int Y();

	public void Iter(double dt);
	public void Draw(Graphics g);
}
