package kanonBall;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;

public class FlisBiter implements IEntity
{
	Random _rng;
	double[] _x;
	double[] _y;
	double[] _vx;
	double[] _vy;

	Color _col;
	
	double _lifeTime = 0;

	public FlisBiter(int x, int y, Color col)
	{
		_rng = new Random();
		_x = new double[128];
		_y = new double[128];
		_vx = new double[128];
		_vy = new double[128];
		_col = col;

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 8; j++) {
				_x[i * 8 + j] = 32 * x + 4 * j;
				_y[i * 8 + j] = 16 * y + 4 * i;
				_vx[i * 8 + j] = 400.0 * 2.0 * (_rng.nextDouble() - 0.5);
				_vy[i * 8 + j] = 400.0 * 1.0 * (_rng.nextDouble());
			}
		}
	}

	public int X()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public int Y()
	{
		// TODO Auto-generated method stub
		return 0;
	}

	public double LifeTime()
	{
		return _lifeTime;
	}
	
	public void Iter(double dt)
	{
		_lifeTime += dt;
		for (int i = 0; i < 128; i++) {
			_vy[i] += dt * 400;

			if (_x[i] < 0) {
				_x[i] = 0.0;
				_vx[i] = -_vx[i];
			}
			if (_x[i] >= 511.0) {
				_x[i] = 511.0;
				_vx[i] = -_vx[i];
			}
			if (_y[i] >= 764.0) {
				_y[i] = 764.0;
				_vy[i] = 0.0;
				_vx[i] *= 0.9;
			}
			// _vx[i] -= 0.01*_vx[i]*_vx[i];

			_x[i] += dt * _vx[i];
			_y[i] += dt * _vy[i];
		}

	}

	public void Draw(Graphics g)
	{
		// Graphics2D g2 = (Graphics2D)g;

		g.setColor(_col);
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 8; j++) {
				if (_vy[i * 8 + j] > 0.0) {
					g.fill3DRect((int) _x[i * 8 + j], (int) _y[i * 8 + j], 4, 4, _rng.nextBoolean());
				} else {
					g.fill3DRect((int) _x[i * 8 + j], (int) _y[i * 8 + j], 4, 4, true);
				}
			}
		}

	}

}
