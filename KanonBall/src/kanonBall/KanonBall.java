package kanonBall;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.Timer;

public class KanonBall extends JPanel implements MouseMotionListener, ActionListener
{

	private Random rng;
	/**
	 * 
	 */
	private static final long serialVersionUID = 8672424960886770506L;
	private int _racket = 0;
	private int _racket_last = 0;
	private Timer _timer;
	private Kule _kule;
	private double _racket_v = 0.0;
	private Color[][] _tiles = null;
	private long lasttimemillis = 0;
	// private FlisBiter _flisbiter = null;
	private LinkedList<FlisBiter> _flisbiter;

	public KanonBall()
	{
		rng = new Random();
		_timer = new Timer(10, this);
		_kule = new Kule(256, 384, 12, Color.WHITE, true);
		_tiles = new Color[16][48];
		_flisbiter = new LinkedList<FlisBiter>();
		Color col;
		lasttimemillis = System.currentTimeMillis();
		for (int x = 0; x < _tiles.length; x++) {
			for (int y = 0; y < _tiles[x].length / 2; y++) {
				// col = new Color(0xff, 0xff,0x0);
				_tiles[x][y] = new Color(rng.nextInt(256), rng.nextInt(256), rng.nextInt(256));
			}
		}
		addMouseMotionListener(this);

		setBackground(Color.BLACK);
		setPreferredSize(new Dimension(512, 768));
		_racket = getWidth() / 2;
		_racket_last = _racket;
		_timer.start();
	}

	public void drawBrick(Graphics g, int x, int y, Color col)
	{
		g.setColor(col);
		g.fill3DRect(32 * x, 16 * y, 32, 16, true);
	}

	public void drawRacket(Graphics g, int x, int s, Color col)
	{
		g.setColor(col);
		g.fill3DRect(x - s / 2, 768 - 16, s, 16, true);
	}

	// Tegning av all grafikk
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		/*
		 * for (int y = 0; y < 768 / 16 / 2; y++) { for (int x = 0; x <
		 * 512 / 32; x++) { drawBrick(g, x, y, Color.GREEN); } }
		 */

		for (int x = 0; x < _tiles.length; x++) {
			for (int y = 0; y < _tiles[x].length; y++) {
				if (_tiles[x][y] != null)
					drawBrick(g, x, y, _tiles[x][y]);
			}
		}
		// drawBall(g, 256, _bally, 24, Color.WHITE);
		_kule.Draw(g);
		if ((_kule.X() >= _racket - 32) && (_kule.X() < _racket + 32)) {
			drawRacket(g, _racket, 64, Color.GREEN);
			if (_kule.Y() > 768 - 16 - _kule.R()) {
				_kule.Turn();
				_kule.Add_vx(_racket_v);
			}
		} else {
			drawRacket(g, _racket, 64, Color.YELLOW);
		}

		for (FlisBiter flisbit : _flisbiter) {
			flisbit.Draw(g);
		}
		/*
		 * if(_flisbiter != null) { _flisbiter.Draw(g); }
		 */

		// g.setColor(Color.GREEN);
		// g.fillRect(10, 10, 32, 16);
		// g.fillRect(10+32+1, 10, 32, 16);
	}

	public void mouseDragged(MouseEvent e)
	{
		// TODO Auto-generated method stub

	}

	public void mouseMoved(MouseEvent e)
	{
		_racket = e.getX();
		repaint();
	}

	public void actionPerformed(ActionEvent e)
	{
		// TODO Auto-generated method stub
		long currenttimemillis = System.currentTimeMillis();
		double dt = (currenttimemillis - lasttimemillis) / 1000.0;
		lasttimemillis = currenttimemillis;
		_racket_v = (_racket - _racket_last);
		_racket_last = _racket;
		_kule.Iter(dt);

		int tx;
		int ty;

		tx = (_kule.X()) / 32;
		ty = (_kule.Y()) / 16;
		if (((tx < _tiles.length) && (tx >= 0)) && (ty < _tiles[0].length) && (ty >= 0)) {
			if (_tiles[tx][ty] != null) {
				_flisbiter.add(new FlisBiter(tx, ty, _tiles[tx][ty]));
				_tiles[tx][ty] = null;
				_kule.Turn();
			}
		}
		if ((_kule.X() >= _racket - 32) && (_kule.X() < _racket + 32)) {
			if (_kule.Y() > 768 - 16 - _kule.R()) {
				_kule.Turn();
				_kule.Add_vx(_racket_v);
			}
		}

		for (FlisBiter flisbit : _flisbiter) {
			flisbit.Iter(dt);
		}
		/*
		 * for(Iterator<FlisBiter> flisbititerator =
		 * _flisbiter.iterator(); flisbititerator.hasNext();
		 * flisbititerator) {
		 * 
		 * }
		 */
		Iterator<FlisBiter> fbi = _flisbiter.iterator();
		while (fbi.hasNext()) {
			if (fbi.next()._lifeTime > 10.0) {
				fbi.remove();
			}
		}

		repaint();
	}
}
